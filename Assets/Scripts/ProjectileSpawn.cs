﻿using UnityEngine;

public class ProjectileSpawn : MonoBehaviour
{
    private bool allowSpawn = true;

    public void Spawn(string projectileName, PlayerController owner)
    {
        if (allowSpawn)
        {
            GameObject projectileObject = PhotonNetwork.Instantiate(projectileName, transform.position, transform.rotation, 0);
            Projectile projectileComponent = projectileObject.GetComponent<Projectile>();
            if (projectileComponent != null)
            {
                projectileComponent.Owner = owner;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        allowSpawn = false;
    }

    private void OnTriggerExit(Collider other)
    {
        allowSpawn = true;
    }
}