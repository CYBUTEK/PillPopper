﻿using UnityEngine;

public class LobbyBackgroundParticles : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera = null;

    private ParticleSystem particles;

    private void Awake()
    {
        particles = GetComponent<ParticleSystem>();
    }

    private void Start()
    {
        if (mainCamera != null && particles != null)
        {
            ParticleSystem.ShapeModule shapeModule = particles.shape;
            Vector3 boxScale = shapeModule.box;
            boxScale.x = Vector3.Distance(mainCamera.ViewportToWorldPoint(new Vector3(0.0f, 0.5f, 10.0f)), mainCamera.ViewportToWorldPoint(new Vector3(1.0f, 0.5f, 10.0f))) * 2.0f;
            boxScale.z = Vector3.Distance(mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.0f, 10.0f)), mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 1.0f, 10.0f))) * 2.0f;

            // particles.transform.position = new Vector3(mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10.0f));
            shapeModule.box = boxScale;
        }
    }
}