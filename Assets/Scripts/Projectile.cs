﻿using Photon;
using UnityEngine;

public class Projectile : PunBehaviour
{
    [SerializeField]
    private float speed = 50.0f;

    [SerializeField]
    private float drop = 0.25f;

    [SerializeField]
    private float elevation = 0.075f;

    [SerializeField]
    private int damage = 15;

    [SerializeField]
    private GameObject splashPrefab = null;

    private Color playerColour;
    private Vector3 previousPosition;

    public PlayerController Owner { get; set; }

    private void FixedUpdate()
    {
        transform.forward = Vector3.Lerp(transform.forward, Vector3.down, Time.deltaTime * drop);

        Vector3 currentPosition = previousPosition + (transform.forward * speed * Time.fixedDeltaTime);

        RaycastHit hitInfo;
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out hitInfo, Vector3.Distance(previousPosition, currentPosition)))
        {
            ProcessHit(hitInfo.collider.gameObject, hitInfo.point, hitInfo.normal);
        }
        else
        {
            transform.position = currentPosition;
            previousPosition = transform.position;
        }
    }

    private void ProcessHit(GameObject hitObject, Vector3 hitPoint, Vector3 hitNormal)
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().enabled = false;

        if (splashPrefab != null)
        {
            SetAllParticlesStartColour(Instantiate(splashPrefab, hitPoint, Quaternion.LookRotation(hitNormal)), playerColour);
        }

        if (photonView.isMine)
        {
            hitObject.SendMessage("AdjustHealth", -damage, SendMessageOptions.DontRequireReceiver);
        }

        enabled = false;
    }

    private void SetAllParticlesStartColour(GameObject gameObj, Color startColour)
    {
        if (gameObj != null)
        {
            foreach (ParticleSystem particles in gameObj.GetComponentsInChildren<ParticleSystem>())
            {
                ParticleSystem.MainModule mainModule = particles.main;
                mainModule.startColor = playerColour;
            }
        }
    }

    private void Start()
    {
        if (Owner != null)
        {
            playerColour = JsonUtility.FromJson<Color>(Owner.photonView.owner.customProperties["colour"] as string);
            SetAllParticlesStartColour(gameObject, playerColour);
        }

        transform.forward = transform.forward + (Vector3.up * elevation);
        previousPosition = transform.position;
    }
}