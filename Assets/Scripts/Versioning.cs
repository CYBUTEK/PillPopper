﻿using System;

public static class Versioning
{
    private static Version application = new Version(1, 1, 0);
    private static Version network = new Version(1, 1, 0);

    public static Version Application
    {
        get
        {
            return application;
        }
    }

    public static Version Network
    {
        get
        {
            return network;
        }
    }
}