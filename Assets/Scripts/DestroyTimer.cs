﻿using Photon;
using UnityEngine;

public class DestroyTimer : PunBehaviour
{
    [SerializeField]
    private float delay = 0.0f;

    [SerializeField]
    private DestroyTypes type = DestroyTypes.Unity;

    public enum DestroyTypes
    {
        Unity,
        Photon
    }

    private void DestroyMe()
    {
        switch (type)
        {
            case DestroyTypes.Unity:
                Destroy(gameObject);
                break;

            case DestroyTypes.Photon:
                PhotonNetwork.Destroy(gameObject);
                break;
        }
    }

    private void Start()
    {
        if (type != DestroyTypes.Photon || photonView.isMine)
        {
            Invoke("DestroyMe", delay);
        }
    }
}