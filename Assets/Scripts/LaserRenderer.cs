﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserRenderer : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        lineRenderer.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        lineRenderer.enabled = true;
    }

    private void RenderLaser()
    {
        if (lineRenderer != null)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(new Ray(transform.position, transform.forward), out hitInfo))
            {
                lineRenderer.SetPositions(new[]
                {
                    transform.position,
                    hitInfo.point
                });
            }
        }
    }

    private void Update()
    {
        RenderLaser();
    }
}