﻿using System;
using Photon;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : PunBehaviour
{
    [SerializeField]
    private GameObject spectatingGuiObject = null;

    [SerializeField]
    private GameObject playingGuiObject = null;

    [SerializeField]
    private Camera spectatorCamera = null;

    [SerializeField]
    private GameObject menuObject = null;

    private PlayerController localPlayerController;

    public static GameManager Instance { get; private set; }

    public bool IsMenuOpen
    {
        get
        {
            return menuObject != null && menuObject.activeSelf;
        }
    }

    public bool IsSpectating
    {
        get
        {
            return spectatingGuiObject != null && spectatingGuiObject.activeSelf;
        }
    }

    public PlayerController LocalPlayerController
    {
        get
        {
            return localPlayerController;
        }

        set
        {
            localPlayerController = value;

            if (spectatingGuiObject != null)
            {
                spectatingGuiObject.SetActive(localPlayerController == null);
            }

            if (playingGuiObject != null)
            {
                playingGuiObject.SetActive(localPlayerController != null);
            }
        }
    }

    public string LocalPlayerName
    {
        get
        {
            return PlayerPrefs.GetString("player_name");
        }

        set
        {
            PlayerPrefs.SetString("player_name", value);
            PhotonNetwork.playerName = value;
        }
    }

    public Camera MainCamera
    {
        get
        {
            if (localPlayerController != null)
            {
                return localPlayerController.Camera;
            }

            return spectatorCamera;
        }
    }

    public static string GetPlayerColorHex(PhotonPlayer player)
    {
        Color playerColour = JsonUtility.FromJson<Color>(player.customProperties["colour"] as string);
        return string.Format("#{0:X2}{1:X2}{2:X2}", Convert.ToByte(playerColour.r * 255.0f), Convert.ToByte(playerColour.g * 255.0f), Convert.ToByte(playerColour.b * 255.0f));
    }

    public void LeaveGame()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();

        SceneManager.LoadScene("Lobby");
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        base.OnPhotonPlayerConnected(player);

        if (PhotonNetwork.isMasterClient)
        {
            ToastManager.Create(ToastManager.JoinedMatchTemplate, GetPlayerColorHex(player), player.name);
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        base.OnPhotonPlayerDisconnected(player);

        if (PhotonNetwork.isMasterClient)
        {
            ToastManager.Create(ToastManager.LeftMatchTemplate, GetPlayerColorHex(player), player.name);
        }
    }

    public void StartPlaying()
    {
        PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity, 0);
    }

    public void StartSpectating()
    {
        if (localPlayerController != null)
        {
            PhotonNetwork.Destroy(localPlayerController.gameObject);
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus && IsMenuOpen == false && IsSpectating == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && menuObject != null)
        {
            menuObject.SetActive(!menuObject.activeSelf);
        }
    }
}