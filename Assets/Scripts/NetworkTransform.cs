﻿using Photon;
using UnityEngine;

public class NetworkTransform : PunBehaviour
{
    public const float SmoothingSpeed = 12.5f;

    [SerializeField]
    private PositionTypes positionType = PositionTypes.Local;

    private Vector3 position;
    private Quaternion rotation;

    public enum PositionTypes
    {
        Local,
        World
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            switch (positionType)
            {
                case PositionTypes.Local:
                    stream.SendNext(transform.localPosition);
                    stream.SendNext(transform.localRotation);
                    break;

                case PositionTypes.World:
                    stream.SendNext(transform.position);
                    stream.SendNext(transform.rotation);
                    break;
            }
        }
        else
        {
            position = (Vector3)stream.ReceiveNext();
            rotation = (Quaternion)stream.ReceiveNext();
        }
    }

    private void Start()
    {
        enabled = photonView != null && photonView.isMine == false && photonView.ObservedComponents.Contains(this);
    }

    private void Update()
    {
        switch (positionType)
        {
            case PositionTypes.Local:
                transform.localPosition = Vector3.Lerp(transform.localPosition, position, Time.deltaTime * SmoothingSpeed);
                transform.localRotation = Quaternion.Lerp(transform.localRotation, rotation, Time.deltaTime * SmoothingSpeed);
                break;

            case PositionTypes.World:
                transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * SmoothingSpeed);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * SmoothingSpeed);
                break;
        }
    }
}