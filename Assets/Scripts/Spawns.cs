﻿using System.Collections.Generic;
using UnityEngine;

public class Spawns : MonoBehaviour
{
    private List<Vector3> spawnPositions = new List<Vector3>();

    public static Spawns Instance { get; private set; }

    public static Vector3 GetRandomSpawnPosition()
    {
        return Instance.spawnPositions[Random.Range(0, Instance.spawnPositions.Count)];
    }

    private void Awake()
    {
        Instance = this;

        foreach (Transform childTransform in transform)
        {
            spawnPositions.Add(childTransform.position);
        }
    }
}