﻿using Photon;
using UnityEngine;

public class ToastManager : PunBehaviour
{
    [SerializeField]
    private GameObject toastPrefab = null;

    [SerializeField]
    private string killedTemplate = string.Empty;

    [SerializeField]
    private string joinedMatchTemplate = string.Empty;

    [SerializeField]
    private string leftMatchTemplate = string.Empty;

    public static ToastManager Instance { get; private set; }

    public static string JoinedMatchTemplate
    {
        get
        {
            return Instance.joinedMatchTemplate;
        }
    }

    public static string KilledTemplate
    {
        get
        {
            return Instance.killedTemplate;
        }
    }

    public static string LeftMatchTemplate
    {
        get
        {
            return Instance.leftMatchTemplate;
        }
    }

    public static void Create(string text)
    {
        if (Instance != null)
        {
            PhotonNetwork.RPC(Instance.photonView, "CreateRpc", PhotonTargets.All, false, text);
        }
    }

    public static void Create(string template, params object[] fields)
    {
        Create(string.Format(template, fields));
    }

    [PunRPC]
    public void CreateRpc(string text)
    {
        if (toastPrefab != null)
        {
            GameObject toastObject = Instantiate(toastPrefab, transform, false);
            if (toastObject != null)
            {
                GuiToast guiToastComponent = toastObject.GetComponent<GuiToast>();
                if (guiToastComponent != null)
                {
                    guiToastComponent.SetText(text);
                }
            }
        }
    }

    private void Awake()
    {
        Instance = this;
    }
}