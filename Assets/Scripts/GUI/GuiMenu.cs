﻿using UnityEngine;
using UnityEngine.UI;

public class GuiMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject leaveGameButtonObject = null;

    [SerializeField]
    private Slider musicVolumeSlider = null;

    [SerializeField]
    private Slider effectsVolumeSlider = null;

    public float EffectsVolume
    {
        get
        {
            if (AudioManager.Instance == null)
            {
                return 1.0f;
            }

            return AudioManager.Instance.EffectsVolume;
        }

        set
        {
            effectsVolumeSlider.value = value;

            if (AudioManager.Instance != null)
            {
                AudioManager.Instance.EffectsVolume = value;
            }
        }
    }

    public float MusicVolume
    {
        get
        {
            if (AudioManager.Instance == null)
            {
                return 1.0f;
            }

            return AudioManager.Instance.MusicVolume;
        }

        set
        {
            musicVolumeSlider.value = value;

            if (AudioManager.Instance != null)
            {
                AudioManager.Instance.MusicVolume = value;
            }
        }
    }

    private void OnDisable()
    {
        if (GameManager.Instance != null && GameManager.Instance.IsSpectating == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;

        if (leaveGameButtonObject != null)
        {
            leaveGameButtonObject.SetActive(GameManager.Instance != null && PhotonNetwork.inRoom);
        }
    }

    private void Start()
    {
        musicVolumeSlider.value = MusicVolume;
        effectsVolumeSlider.value = EffectsVolume;
    }
}