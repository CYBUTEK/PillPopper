﻿using UnityEngine;
using UnityEngine.UI;

public class GuiRoomList : MonoBehaviour
{
    [SerializeField]
    private GameObject roomListItemPrefab = null;

    public void Refresh()
    {
        RoomInfo[] roomInfos = PhotonNetwork.GetRoomList();

        print(string.Format("Refreshing {0} Rooms", roomInfos.Length));

        foreach (Transform childTransform in transform)
        {
            Destroy(childTransform.gameObject);
        }

        foreach (RoomInfo roomInfo in roomInfos)
        {
            GameObject roomListItemObject = Instantiate(roomListItemPrefab, transform, false);
            if (roomListItemObject != null)
            {
                GuiRoomListItem roomListItemComponent = roomListItemObject.GetComponent<GuiRoomListItem>();
                if (roomListItemComponent != null)
                {
                    roomListItemComponent.RoomInfo = roomInfo;
                }
            }
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
    }
}