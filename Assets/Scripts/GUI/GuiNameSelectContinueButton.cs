﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GuiNameSelectContinueButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private GameObject thisScreen = null;

    [SerializeField]
    private GameObject nextScreen = null;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (string.IsNullOrEmpty(LobbyManager.Instance.PlayerName) == false)
        {
            if (thisScreen != null)
            {
                thisScreen.SetActive(false);
            }

            if (nextScreen != null)
            {
                nextScreen.SetActive(true);
            }
        }
    }
}