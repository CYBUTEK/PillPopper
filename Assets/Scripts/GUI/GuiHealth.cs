﻿using UnityEngine;
using UnityEngine.UI;

public class GuiHealth : MonoBehaviour
{
    private Image imageComponent;
    private Text textComponent;

    private void Awake()
    {
        imageComponent = GetComponentInChildren<Image>();
        textComponent = GetComponentInChildren<Text>();
    }

    private void Update()
    {
        if (GameManager.Instance.LocalPlayerController != null && imageComponent != null && textComponent != null)
        {
            imageComponent.fillAmount = Mathf.Lerp(imageComponent.fillAmount, GameManager.Instance.LocalPlayerController.Health / 100.0f, Time.deltaTime * 10.0f);
            textComponent.text = Mathf.RoundToInt(imageComponent.fillAmount * 100.0f).ToString();
        }
    }
}