﻿using UnityEngine;
using UnityEngine.UI;

public class GuiVersionLabel : MonoBehaviour
{
    private Text textComponent;

    private void Awake()
    {
        textComponent = GetComponent<Text>();
    }

    private void Start()
    {
        if (textComponent != null)
        {
            textComponent.text = string.Format("Application:{0} | Network:{1}", Versioning.Application, Versioning.Network);
        }
    }
}