﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GuiRoomListItem : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Text nameTextComponent = null;

    [SerializeField]
    private Text countTextComponent = null;

    [SerializeField]
    private string countFormat = "{0} / {1}";

    private int maxPlayers;
    private int playerCount;

    private RoomInfo roomInfo;

    public int MaxPlayers
    {
        get
        {
            return maxPlayers;
        }

        set
        {
            maxPlayers = value;
            RefreshCountText();
        }
    }

    public int PlayerCount
    {
        get
        {
            return playerCount;
        }

        set
        {
            playerCount = value;
            RefreshCountText();
        }
    }

    public RoomInfo RoomInfo
    {
        get
        {
            return roomInfo;
        }

        set
        {
            roomInfo = value;
            Refresh();
        }
    }

    public string RoomName
    {
        get
        {
            if (nameTextComponent != null)
            {
                return nameTextComponent.text;
            }

            return string.Empty;
        }

        set
        {
            if (nameTextComponent != null)
            {
                nameTextComponent.text = value;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        LobbyManager.Instance.SetJoinOrCreateRoomInputField(RoomName);
    }

    public void Refresh()
    {
        if (roomInfo != null)
        {
            RoomName = roomInfo.name;
            MaxPlayers = roomInfo.maxPlayers;
            PlayerCount = roomInfo.playerCount;
        }
    }

    private void RefreshCountText()
    {
        if (countTextComponent != null)
        {
            countTextComponent.text = string.Format(countFormat, playerCount, maxPlayers);
        }
    }
}