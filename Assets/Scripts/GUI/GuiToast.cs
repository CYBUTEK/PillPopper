﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GuiToast : MonoBehaviour
{
    [SerializeField]
    private float showDuration = 3.0f;

    private CanvasGroup canvasGroupComponent;
    private Text textComponent;

    public void SetText(string text)
    {
        if (textComponent != null)
        {
            textComponent.text = text;
        }
    }

    private void Awake()
    {
        canvasGroupComponent = GetComponent<CanvasGroup>();
        textComponent = GetComponentInChildren<Text>();
    }

    private void FadeOut()
    {
        StartCoroutine(FadeOutCoroutine());
    }

    private IEnumerator FadeOutCoroutine()
    {
        if (canvasGroupComponent != null)
        {
            canvasGroupComponent.alpha = 0.0f;

            float progress = 0.0f;
            while (progress <= 1.0f)
            {
                progress = progress + Time.deltaTime;
                canvasGroupComponent.alpha = Mathf.Lerp(1.0f, 0.0f, progress);
                yield return null;
            }
        }

        Destroy(gameObject);
    }

    private IEnumerator Start()
    {
        if (canvasGroupComponent != null)
        {
            canvasGroupComponent.alpha = 0.0f;

            float progress = 0.0f;
            while (progress <= 1.0f)
            {
                progress = progress + Time.deltaTime;
                canvasGroupComponent.alpha = Mathf.Lerp(0.0f, 1.0f, progress);
                yield return null;
            }
        }

        Invoke("FadeOut", showDuration);
    }
}