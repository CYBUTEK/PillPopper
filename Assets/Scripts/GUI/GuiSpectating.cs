﻿using UnityEngine;
using UnityEngine.UI;

public class GuiSpectating : MonoBehaviour
{
    [SerializeField]
    private InputField nameInputFieldComponent = null;

    [SerializeField]
    private Camera spectatorCamera = null;

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.Locked;

        if (spectatorCamera != null)
        {
            spectatorCamera.gameObject.SetActive(false);
        }
    }

    private void OnEnabled()
    {
        Cursor.lockState = CursorLockMode.None;

        if (spectatorCamera != null)
        {
            spectatorCamera.gameObject.SetActive(true);
        }
    }

    private void Start()
    {
        if (nameInputFieldComponent != null)
        {
            nameInputFieldComponent.text = GameManager.Instance.LocalPlayerName;
        }
    }
}