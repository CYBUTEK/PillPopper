﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GuiPlayerScoreList : MonoBehaviour
{
    [SerializeField]
    private GameObject playerScoreItemPrefab = null;

    public static GuiPlayerScoreList Instance { get; private set; }

    public void Refresh()
    {
        foreach (Transform childTransform in transform)
        {
            Destroy(childTransform.gameObject);
        }

        if (playerScoreItemPrefab != null)
        {
            PlayerController[] playerControllers = FindObjectsOfType<PlayerController>().OrderByDescending(playerController => playerController.Kills - playerController.Deaths).ToArray();

            for (int i = 0; i < playerControllers.Length; i++)
            {
                PlayerController playerController = playerControllers[i];
                if (playerController != null)
                {
                    GameObject playerScoreItemObject = Instantiate(playerScoreItemPrefab, transform, false);
                    if (playerScoreItemObject != null)
                    {
                        Text textComponent = playerScoreItemObject.GetComponent<Text>();
                        if (textComponent != null)
                        {
                            textComponent.text = string.Format(
                                "<color={4}>{0} - Score:{1} / K:{2} / D:{3}</color>",
                                playerController.photonView.owner.name,
                                playerController.Score,
                                playerController.Kills,
                                playerController.Deaths,
                                GameManager.GetPlayerColorHex(playerController.photonView.owner));
                        }
                    }
                }
            }
        }
    }

    private void Awake()
    {
        Instance = this;
    }
}