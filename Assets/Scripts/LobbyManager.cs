﻿using Photon;
using UnityEngine;
using UnityEngine.UI;

public class LobbyManager : PunBehaviour
{
    [SerializeField]
    private InputField joinOrCreateRoomInputField = null;

    [SerializeField]
    private InputField playerNameInputField = null;

    [SerializeField]
    private GuiRoomList guiRoomList = null;

    [SerializeField]
    private GameObject menuObject = null;

    public static LobbyManager Instance { get; private set; }

    public string JoinOrCreateRoomName { get; set; }

    public string PlayerName
    {
        get
        {
            return PlayerPrefs.GetString("player_name");
        }

        set
        {
            PlayerPrefs.SetString("player_name", value);
            PhotonNetwork.playerName = value;
        }
    }

    public void JoinOrCreateRoom()
    {
        if (string.IsNullOrEmpty(JoinOrCreateRoomName))
        {
            JoinOrCreateRoomName = "n00bs Welc0me!";
        }

        PhotonNetwork.JoinOrCreateRoom(
            JoinOrCreateRoomName,
            new RoomOptions
            {
                MaxPlayers = 8
            },
            TypedLobby.Default);
    }

    public override void OnConnectedToPhoton()
    {
        base.OnConnectedToPhoton();

        print("Joined Server");
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        print("Joined Lobby");

        PhotonNetwork.player.customProperties["colour"] = JsonUtility.ToJson(Random.ColorHSV(0.0f, 1.0f, 0.5f, 0.5f, 1.0f, 1.0f));
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        print("Joined Room");

        PhotonNetwork.LoadLevel("Arena");
    }

    public override void OnReceivedRoomListUpdate()
    {
        base.OnReceivedRoomListUpdate();

        RefreshRoomList();
    }

    public void RefreshRoomList()
    {
        if (guiRoomList != null)
        {
            guiRoomList.Refresh();
        }
    }

    public void SetJoinOrCreateRoomInputField(string text)
    {
        if (joinOrCreateRoomInputField != null)
        {
            joinOrCreateRoomInputField.text = text;
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;

        if (playerNameInputField != null)
        {
            playerNameInputField.text = PlayerName;
        }

        if (PhotonNetwork.connected == false || PhotonNetwork.connecting == false)
        {
            PhotonNetwork.automaticallySyncScene = true;
            PhotonNetwork.autoJoinLobby = true;
            PhotonNetwork.ConnectUsingSettings(Versioning.Network.ToString());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && menuObject != null)
        {
            menuObject.SetActive(!menuObject.activeSelf);
        }
    }
}