﻿using System.Linq;
using Photon;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : PunBehaviour
{
    public const float Gravity = 20.0f;
    public const float JumpForce = 10.0f;
    public const float MaxSpeed = 10.0f;
    public const float MoveDamping = 0.25f;
    public const float MoveForce = 3000.0f;

    [SerializeField]
    private Transform armPivot = null;

    [SerializeField]
    private ProjectileSpawn projectileSpawn = null;

    [SerializeField]
    private Canvas statsCanvas = null;

    [SerializeField]
    private Image healthImage = null;

    [SerializeField]
    private Text healthText = null;

    [SerializeField]
    private Text playerNameText = null;

    private new Rigidbody rigidbody;
    private float verticalLookAngle;

    public Camera Camera { get; private set; }

    public int Deaths { get; private set; }

    public int Health { get; private set; }

    public int Kills { get; private set; }

    public int Score
    {
        get
        {
            return Kills - Deaths;
        }
    }

    public void AddKill()
    {
        photonView.RPC("AddKillRpc", PhotonTargets.AllBuffered);
    }

    public void AdjustHealth(int amount)
    {
        photonView.RPC("AdjustHealthRpc", PhotonTargets.AllBuffered, amount, PhotonNetwork.player.ID);
    }

    [PunRPC]
    private void AddDeathRpc()
    {
        Deaths++;
    }

    [PunRPC]
    private void AddKillRpc()
    {
        Kills++;
        GuiPlayerScoreList.Instance.Refresh();
    }

    [PunRPC]
    private void AdjustHealthRpc(int amount, int playerId)
    {
        Health = Mathf.Clamp(Health + amount, 0, 100);

        if (photonView.isMine && Health == 0)
        {
            PhotonPlayer otherPhotonPlayer = PhotonNetwork.otherPlayers.First(player => player.ID == playerId);
            ToastManager.Create(ToastManager.KilledTemplate, GameManager.GetPlayerColorHex(photonView.owner), photonView.owner.name, GameManager.GetPlayerColorHex(otherPhotonPlayer), otherPhotonPlayer.name);
            photonView.RPC("AddDeathRpc", PhotonTargets.AllBuffered);

            PlayerController otherPlayerController = FindObjectsOfType<PlayerController>().First(playerController => playerController.photonView.owner.ID == playerId);
            if (otherPlayerController != null)
            {
                otherPlayerController.AddKill();
            }

            Respawn();
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        Camera = GetComponentInChildren<Camera>();
    }

    private void FixedUpdate()
    {
        if (photonView.isMine && GameManager.Instance.IsMenuOpen == false)
        {
            bool isGrounded = GetIsGrounded();
            float moveForce = isGrounded ? MoveForce : MoveForce * 0.5f;

            Vector3 localVelocity = transform.InverseTransformVector(rigidbody.velocity);

            if (Input.GetKey(KeyCode.W))
            {
                rigidbody.AddRelativeForce(Vector3.forward * moveForce);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                rigidbody.AddRelativeForce(Vector3.back * moveForce);
            }
            else if (localVelocity.z < 2.0f || localVelocity.z > 2.0f)
            {
                rigidbody.AddRelativeForce(Vector3.back * moveForce * localVelocity.z * MoveDamping);
            }

            if (Input.GetKey(KeyCode.A))
            {
                rigidbody.AddRelativeForce(Vector3.left * moveForce);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                rigidbody.AddRelativeForce(Vector3.right * moveForce);
            }
            else if (localVelocity.x < 2.0f || localVelocity.x > 2.0f)
            {
                rigidbody.AddRelativeForce(Vector3.left * moveForce * localVelocity.x * MoveDamping);
            }

            if (isGrounded == false)
            {
                rigidbody.AddForce(Vector3.down * Gravity, ForceMode.Acceleration);
            }

            Vector3 lateralVelocity = rigidbody.velocity;
            lateralVelocity.y = 0.0f;
            if (lateralVelocity.magnitude > MaxSpeed)
            {
                lateralVelocity = lateralVelocity.normalized * MaxSpeed;
                rigidbody.velocity = new Vector3(lateralVelocity.x, rigidbody.velocity.y, lateralVelocity.z);
            }
        }
    }

    private bool GetIsGrounded()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(new Ray(transform.position, -transform.up), out hitInfo) && hitInfo.distance < 1.1f)
        {
            return true;
        }

        return false;
    }

    private void OnDestroy()
    {
        if (GuiPlayerScoreList.Instance != null)
        {
            GuiPlayerScoreList.Instance.Refresh();
        }
    }

    private void Respawn()
    {
        if (photonView.isMine)
        {
            AdjustHealth(100);
            transform.position = Spawns.GetRandomSpawnPosition();
        }
    }

    private void Start()
    {
        if (photonView.isMine)
        {
            GameManager.Instance.LocalPlayerController = this;
        }

        if (playerNameText != null)
        {
            playerNameText.text = photonView.owner.name;
        }

        Camera.gameObject.SetActive(photonView.isMine);
        Respawn();

        if (GuiPlayerScoreList.Instance != null)
        {
            GuiPlayerScoreList.Instance.Refresh();
        }

        GetComponent<MeshRenderer>().material.color = JsonUtility.FromJson<Color>(photonView.owner.customProperties["colour"] as string);
    }

    private void Update()
    {
        if (photonView.isMine && GameManager.Instance.IsMenuOpen == false)
        {
            verticalLookAngle = Mathf.Clamp(verticalLookAngle - Input.GetAxis("Mouse Y"), -60.0f, 60.0f);
            Vector3 localEulerAngles = Camera.transform.localEulerAngles;
            localEulerAngles.x = verticalLookAngle;
            Camera.transform.localEulerAngles = localEulerAngles;
            transform.Rotate(Vector3.up, Input.GetAxis("Mouse X"), Space.Self);

            if (armPivot != null)
            {
                armPivot.localRotation = Camera.transform.localRotation;
            }

            if (Input.GetButtonDown("Jump") && GetIsGrounded())
            {
                rigidbody.AddRelativeForce(Vector3.up * JumpForce, ForceMode.VelocityChange);
            }

            UpdateShooting();
        }
        else
        {
            if (statsCanvas != null)
            {
                statsCanvas.transform.rotation = GameManager.Instance.MainCamera.transform.rotation;
            }

            if (healthImage != null && healthText != null)
            {
                healthImage.fillAmount = Mathf.Lerp(healthImage.fillAmount, Health / 100.0f, Time.deltaTime * 10.0f);
                healthText.text = Mathf.RoundToInt(healthImage.fillAmount * 100.0f).ToString();
            }
        }
    }

    private void UpdateShooting()
    {
        if (Input.GetMouseButtonDown(0) && projectileSpawn != null)
        {
            projectileSpawn.Spawn("Paintball", this);
        }
    }
}