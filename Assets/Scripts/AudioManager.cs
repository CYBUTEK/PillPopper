﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioMixer audioMixer = null;

    public static AudioManager Instance { get; private set; }

    public float EffectsVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("effects_volume", 1.0f);
        }

        set
        {
            audioMixer.SetFloat("effects_volume", ToDecibels(value));
            PlayerPrefs.SetFloat("effects_volume", value);
        }
    }

    public float MusicVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("music_volume", 1.0f);
        }

        set
        {
            audioMixer.SetFloat("music_volume", ToDecibels(value));
            PlayerPrefs.SetFloat("music_volume", value);
        }
    }

    public static float ToDecibels(float volume)
    {
        return Mathf.Clamp(10.0f * Mathf.Log(volume, 2.0f), -80.0f, 0.0f);
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        MusicVolume = MusicVolume;
        EffectsVolume = EffectsVolume;
    }
}